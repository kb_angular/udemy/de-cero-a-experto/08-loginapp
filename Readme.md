#   LOGIN APP   
##  Login tradicional y manejo de tokens - Firebase

    Importar el proyecto base:
        https://github.com/Klerith/angular-login-base/releases

        luego instalar los paquetes de node
        $ npm install
    
## Formulario por aprox Template

###  Model
    Modelar nuestra clase

### ngMOdel
    camputar los valores que se envia por formulario.

### ngSubmit
    Envento para enviar informacion a traves de un formulario.

### #f="ngForm"
    Asginacion de todas las propiedades del formularios, lo cual nos permitira realizar la validaciones

    Validacoines de ngForm
    email       --> el valor tiene que ser de tipo email.
    required    --> el campo es obligatorio
    minlength="6" > logintud de una atributo.


    Resultado de ngForm
    controls --> muestra los valores que se envia con sus respectivos propiedades
    invalid    --> valor booleano, esta enlazado con lo parametros de validacion

## Formulario aprox Reactivo
## Formulario aprox Modelo


## Firebase
    Para realizar el Backend, en donde vamos a almacenar nuestra data.
    https://firebase.google.com/

    Metodo de autenticacion
    https://console.firebase.google.com/project/login-app-47045/authentication/users

    Comumir el Servicio REST
    https://firebase.google.com/docs/reference/rest/auth?authuser=0

    Consumiremos el servicio
        Crear usuario
        -   Sign up with email / password

        Autenticarse
        -   Sign in with email / password   

## SweetAlert2
    https://sweetalert2.github.io/#download

    Inslacion 
    # npm install sweetalert2
    
    o por CDN

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>



## Guard
    Para proteger nuestro web, para usuarios que no se autentican
    # ng g guard guards/auth