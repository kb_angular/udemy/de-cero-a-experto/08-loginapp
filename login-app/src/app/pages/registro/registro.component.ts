import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../../services/auth.service';
import { UserModel } from '../../model/user.model';
// ES6 Modules or TypeScript
// import Swal from 'sweetalert2';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  // Instanciamos nuesta clase
  userModel: UserModel;

  remenber = false;

  constructor( private auth: AuthService,
               private router: Router) { }

  ngOnInit() {
    // Inicializamos la clase.
    this.userModel = new UserModel();
   }

   onSubmit( form: NgForm) {
    if ( form.invalid ) { return; }

    Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Espere por favor...'
    });
    // ELimina el boton  y en su lugar muestra carga
    Swal.showLoading();


    this.auth.newUser(this.userModel)
        .subscribe( resp => {
          console.log(resp);

          Swal.close();
          
          if ( this.remenber ){
            localStorage.setItem('email', this.userModel.email );
          }


          this.router.navigateByUrl('/home');

        }, (err) => {
          console.log("Obteniento Error");
          console.log(err.error.error.message);

          Swal.fire({
            title: 'Error en el Registro',
            icon: 'error',
            text: err.error.error.message
          });
        });
   }


}
