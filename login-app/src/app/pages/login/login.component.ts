import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { UserModel } from '../../model/user.model';
import { AuthService } from '../../services/auth.service';
// ES6 Modules or TypeScript
// import Swal from 'sweetalert2';
import Swal from 'sweetalert2/dist/sweetalert2.js';




@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userModel: UserModel;
  remenber = false;

  constructor(private auth: AuthService,
              private router: Router) { }

  ngOnInit() {
    this.userModel = new UserModel();

    if ( localStorage.getItem( 'email' ) ){
      this.userModel.email = localStorage.getItem( 'email' );
      this.remenber = true;
    }
  }

  onSubmit( form: NgForm ) {
    if ( form.invalid ) { return; }

    // console.log(form);

    Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Espere por favor...'
    });
    // ELimina el boton  y en su lugar muestra carga
    Swal.showLoading();

    this.auth.login(this.userModel)
             .subscribe(resp => {
               console.log(resp);

               Swal.close();

               if ( this.remenber ){
                 localStorage.setItem('email', this.userModel.email );
               }

               this.router.navigateByUrl('/home');

             }, (err) => {
               console.log(err.error.error.message);

               Swal.fire({
                title: 'Error en la Autenticacion',
                icon: 'error',
                text: err.error.error.message
              });
             });
    }

}
