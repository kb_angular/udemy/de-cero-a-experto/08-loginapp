import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserModel } from '../model/user.model';
import { map } from 'rxjs/operators';




@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url = "https://identitytoolkit.googleapis.com/v1";
  private apiKey = "AIzaSyCqTJNARlkU_BXM6zbkWoiVEXD1h9OEh9g";

  userToken: string;

  // Crear un usuario
  // https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=[API_KEY]
  

  // Para autenticarse
  // https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=[API_KEY]



  constructor( private http: HttpClient) { 
    this.readToken();
  }

  logout() {
    // token --> es la variable que hemos enviado a localStoge
    localStorage.removeItem('token');
  }

  login( user: UserModel ) {
    const authData = {
      // ... es el operador Spread --> usamos ello xq tenemos el mismo nombre de la propiedad.
      ...user,
      returnSecureToken: true
    };
    return this.http.post(
      `${ this.url }/accounts:signInWithPassword?key=${ this.apiKey }`, authData )
      .pipe(
        map(resp => {
          console.log( 'Entro en el map del RXJS' );

          this.saveToken( resp[ 'idToken' ]);
          return resp;
        })
        );
  }

  newUser( user: UserModel ) {
    const authData = {
      // ... es el operador Spread --> usamos ello xq tenemos el mismo nombre de la propiedad.
      ...user,
      returnSecureToken: true
    };

    return this.http.post(
      `${ this.url }/accounts:signUp?key=${ this.apiKey }`, authData )
      .pipe(
        map(resp => {
          console.log( 'Entro en el map del RXJS' );
          this.saveToken( resp[ 'idToken' ]);
          return resp;
        })
        );


  }
  // guardamos el tocken en localStorage
  private saveToken( idTocken: string) {
    this.userToken = idTocken;
    localStorage.setItem('token', idTocken);

    let hoy = new Date();
    hoy.setSeconds( 3600 );

    localStorage.setItem('expira', hoy.getTime().toString() );
  }

  private readToken() {
    if ( localStorage.getItem( 'token' )){
      this.userToken = localStorage.getItem( 'token' );
    } else {
      this.userToken = '';
    }

    return this.userToken;
  }


  isAuthenticate(): boolean{
    if ( this.userToken.length < 2 ) {
      return false;
    }
    
    // Nimber --> debido a que setTime recibe como parametro entero
    const expira = Number( localStorage.getItem('expira') );
    const expiraDate = new Date();

    expiraDate.setTime( expira );

    if ( expiraDate > new Date() ) {
      return true;
    } else {
      return false;
    }
    
  }
}
